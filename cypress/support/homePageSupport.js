Cypress.Commands.add('clickOnItem', (news) => { 
    cy.get('.css-mhc70k-MuiGrid-root').find('.css-1744jwa-MuiGrid-root:visible').each(($el, index, $list) => {
        const text = $el.find('.MuiTypography-root').text();
        console.log(text)
        if(text.includes(news)){
            cy.wrap($el).click();
        }
    })
 })

 Cypress.Commands.add('clickOnItemAndBack', (item) => { 
    cy.get('.css-mhc70k-MuiGrid-root').find('.css-1744jwa-MuiGrid-root:visible').each(($el, index, $list) => {
        const text = $el.find('.MuiTypography-root').text();
        console.log(text)
        if(text.includes(item)){
            cy.wrap($el).click();
        }
    })
    cy.get('[test_id="homePage"]').click();
 })