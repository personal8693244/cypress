import LoginPage from '../pageObjects/loginPageObject'

const loginPage = new LoginPage();

describe('Lecturer page', () => {

    let user;
    before( () => {
        cy.visit(Cypress.env('conferenceURL'), { failOnStatusCode: false });
        cy.fixture('conferenceUser').then((conferenceUser) =>{
            user=conferenceUser;
            loginPage.getUserName().type(user.email);
            loginPage.getPasswrod().type(user.pass);
            loginPage.getLogInButton().click();
        });
        cy.clickOnItem("Lecturer")
    });

    beforeEach(function() {
        
    })
    
    it('Lecturer page', () => {
        cy.wait(1000);
        console.log("JOCACAR");
        var text = '';
        cy.get('.css-1ex1afd-MuiTableCell-root:first-child').each(($li, index, $lis) => {
            const elem = $li;
            text = text + elem.text(); 
        }).then(function(){
            console.log(text);
        })
    })
  })