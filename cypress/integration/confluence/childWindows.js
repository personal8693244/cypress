describe('Child Window', () => {
    
    it('My child window', () => {
        cy.visit("https://rahulshettyacademy.com/AutomationPractice");
        //instead of open new tab, target is removed so, new page will be loaded in the same tab
        cy.get("#opentab").invoke('removeAttr', 'target').click();

        //because of this command, cy will work on new page
        cy.origin("https://www.qaclickacademy.com/",() =>{
            cy.get("#navbarSupportedContent a[href$='about.html']").click()
            cy.get("#about-page h2").should('have.text', 'Welcome to QAClick Academy ');
        });

        
    })
  })