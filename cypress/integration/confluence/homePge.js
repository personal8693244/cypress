
describe('Conference Home Page', () => {

    let user;
    before( () => {
        cy.fixture('conferenceUser').then((conferenceUser) =>{
            user=conferenceUser;
        });
    });
    
    it('Home Page', () => {
        cy.visit('http://www.admin-portal.levi9-test.s3-website.eu-central-1.amazonaws.com/login', { failOnStatusCode: false });
        //id
        cy.get("#username").type("aleksa.bogdanovic@levi9.com");
        cy.get("#password").type("test1234");
        //attribute
        cy.get('[test_id="loginButton"]').click();
        //only visible buttons
        cy.get('button:visible').click();
        cy.get('body div div h4').should('have.text', 'Home Page');
        cy.get('.css-1744jwa-MuiGrid-root:visible').should('have.length', 8);
        cy.get('.css-mhc70k-MuiGrid-root').as('itemsOnPage');

        //another way for above examoke
        cy.get('@itemsOnPage').find('.css-1744jwa-MuiGrid-root:visible').should('have.length', 8);
       //cy.wait(2000);
        //Check text on specific elemnt
        cy.get('@itemsOnPage').find('.css-1744jwa-MuiGrid-root:visible').eq(1).contains('Events');
        //Click on the same product
        //cy.get('@itemsOnPage').find('.css-1744jwa-MuiGrid-root:visible').eq(1).contains('Events').click();
        cy.clickOnItem('News');
    
        //cy.pause();
        //cy.debug();

        cy.get('.MuiTypography-root').then(function(logoelement)
        {
            //log into web console
            console.log(logoelement.text());
            //log into cypress window
            cy.log(logoelement.text());
            cy.get('.css-1dxc53o-MuiButtonBase-root-MuiButton-root').click();
        })

        user.itemName.forEach(function(element){
            cy.clickOnItemAndBack(element);
            cy.wait(1000)
        })
    })
  })