import LoginPage from '../pageObjects/loginPageObject'

describe('Conference login', () => {
    let user;
    const loginPage = new LoginPage();

    before( () => {
        cy.fixture('conferenceUser').then((conferenceUser) =>{
            user=conferenceUser;
        });
    });

    beforeEach(function() {
        cy.visit(Cypress.env('conferenceURL'), { failOnStatusCode: false });
        
        loginPage.getUserName().type(user.email);
        loginPage.getPasswrod().type(user.pass);
        loginPage.getLogInButton().click();
    })
    
    it('Login ',{defaultCommandTimeout: 100000 }, () => {
        //Cypress.config();
        loginPage.getPageTitle().should('have.text', 'Home Page');
    })

    it('Logout ', () => {
        //Cypress.config('defaultCommandTimeout', 50);
        loginPage.getLogOutButton().click();
        loginPage.getPageTitle().should('have.text', 'Log In');
        loginPage.getPasswrod().should('have.attr', 'id', "password")
    })

  })