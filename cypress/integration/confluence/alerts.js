describe('Alerts', () => {
    it('My Alerts', () => {
        
        cy.visit("http://qaclickacademy.com/practice.php");
        cy.get('#alertbtn').click();
        cy.get('#confirmbtn').click();

        //window:alert
        cy.on('window:alert', (str)=>
        {
            //Mocha
            expect(str).to.equal('Hello , share this practice page and share your knowledge');
            expect(str.includes('Hello')).to.be.true;
        })

        cy.on('window:confirm', (str)=>
        {
            expect(str).to.equal('Hello , Are you sure you want to confirm?');
        })

        //instead of new tab open, force opening url on same tap, because of cypress limitation about hendling new tab 
       // cy.get('#opentab').invoke('removeAttr', 'target').click();

        //cy.url().should('include','qaclickacademy')
        
        //browser navegation back
        //cy.go('back');
    })
  })