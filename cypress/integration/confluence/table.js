describe('Table', () => {
    
    it('My table', () => {
        cy.visit('http://www.admin-portal.levi9-test.s3-website.eu-central-1.amazonaws.com/login', { failOnStatusCode: false });
        //id
        cy.get("#username").type("aleksa.bogdanovic@levi9.com");
        cy.get("#password").type("test1234");
        //attribute
        cy.get('[test_id="loginButton"]').click();
       
        //Click on the same product
        cy.get('.css-mhc70k-MuiGrid-root').find('.css-1744jwa-MuiGrid-root:visible').eq(2).contains('Lecturers').click();

        cy.get('tbody tr td:nth-child(1)').each(($el, index, $list) => {
            const text = $el.find('strong').text();
            if(text.includes('5Mp2tV65lp6')) {
                cy.get('[test_id="tableName"]').eq(index).then(function(name){
                    const nameOnScreen = name.text()
                    console.log(nameOnScreen);
                })
                cy.get('[data-testid="EditIcon"]').eq(index).click();
            }
            console.log(text)
        })
       
    })
  })