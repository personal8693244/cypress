describe('Checkbox', () => {
    it('My checkBox', () => {
        cy.visit("https://rahulshettyacademy.com/AutomationPractice");

        //Checkbox
        cy.get('#checkBoxOption1').check().should('be.checked').and('have.value', 'option1');
        cy.get('#checkBoxOption1').uncheck().should('not.be.checked');
        cy.get('input[type="checkbox"]').check(['option2', 'option3']);

        //Static dropdown
        cy.get('select').select('option2').should('have.value', 'option2');

        //Dynamic dropdown
        cy.get('#autocomplete').type('ind')
        
        //Autocomplete
        //parent -> .ui-menu-item
        //child -> div
        cy.get('.ui-menu-item div').each(($li, index, $lis) => {

           if($li.text() === 'India'){
                cy.wrap($li).click();
           }
        })
        cy.get('#autocomplete').should('have.value', 'India');

        //Visible unvisible
        cy.get('#displayed-text').should('be.visible');
        cy.get('#hide-textbox').click();
        cy.get('#displayed-text').should('not.be.visible');
        cy.get('#show-textbox').click();
        cy.get('#displayed-text').should('be.visible');

        //Radio buttons
        cy.get('[value="radio2"]').check().should('be.checked');

    })
  })