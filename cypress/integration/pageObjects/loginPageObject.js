class loginPageObject {
    getUserName() {
        return cy.get('#username');
    }

    getPasswrod() {
        return cy.get( '#password');
    }

    getLogInButton() {
        return cy.get('[test_id="loginButton"]');
    }

    getLogOutButton() {
        return cy.get('[test_id="logout"]');
    }

    getPageTitle() {
        return cy.get('body div div h4');
    }
}
export default loginPageObject;