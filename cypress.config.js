const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: '4gegnj',
  defaultCommandTimeout: 6000,
  env: {
    conferenceURL: 'http://www.admin-portal.levi9-test.s3-website.eu-central-1.amazonaws.com/login'
  },
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    specPattern: 'cypress/integration/confluence'
  }
});
